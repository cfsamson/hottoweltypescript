﻿var Application;
(function (Application) {
    (function (Controllers) {
        var controllerId = 'dashboard';
        angular.module('app').controller(controllerId, [
            'common', 'datacontext',
            function (common, datacontext) {
                return new Application.Controllers.Dashboard(common, datacontext);
            }
        ]);

        var Dashboard = (function () {
            //#endregion
            function Dashboard(common, datacontext) {
                this.people = [];
                this.common = common;
                this.datacontext = datacontext;
                this.log = common.logger.getLogFn(controllerId);
                this.news = this.getNews();

                // Queue all promises and wait for them to finish before loading the view
                this.activate([this.getMessageCount(), this.getPeople()]);
            }
            // TODO: is there a more elegant way of activating the controller - base class?
            Dashboard.prototype.activate = function (promises) {
                var _this = this;
                this.common.activateController(promises, controllerId).then(function () {
                    _this.log('Activated Dashboard View');
                });
            };

            //#region Public Methods
            Dashboard.prototype.getNews = function () {
                return {
                    title: "Hot Towel Typescript",
                    description: 'Hot Towel Typescript is a SPA template using Angular, Breeze and Typescript. ' + 'This is a conversion of John Papas HotTowel.Angular.Breeze package'
                };
            };

            Dashboard.prototype.getMessageCount = function () {
                var _this = this;
                return this.datacontext.getMessageCount().then(function (data) {
                    return _this.messageCount = data;
                });
            };

            Dashboard.prototype.getPeople = function () {
                var _this = this;
                return this.datacontext.getPeople().then(function (data) {
                    return _this.people = data;
                });
            };
            return Dashboard;
        })();
        Controllers.Dashboard = Dashboard;
    })(Application.Controllers || (Application.Controllers = {}));
    var Controllers = Application.Controllers;
})(Application || (Application = {}));
//# sourceMappingURL=dashboard.js.map
