﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
'use strict';
module Application.Shared
{
    
    //#region explanation
    //-------STARTING COMMON MODULE----------
    // NB! script for this file must get loaded before the "child" script files

    // THIS CREATES THE ANGULAR CONTAINER NAMED 'common', A BAG THAT HOLDS SERVICES
    // CREATION OF A MODULE IS DONE USING ...module('moduleName', []) => retrieved using ...module.('...')
    // Contains services:
    //  - common
    //  - logger
    //  - spinner
    //#endregion
    var commonModule: ng.IModule = angular.module('common', []);

    //#region explanation
    // THIS IS A PROVIDER THAT ALLOWS YOU TO SET CONFIGURATIONS THAT ARE RUN INN CONFIG_PHASE, BEFORE
    // THE APPLICATION LIFE-CYCLE ENTERS THE RUN PHASE AND SERVICES ARE INSTANSIATED
    // Must configure the common service and set its 
    // events via the commonConfigProvider
    //#endregion
    commonModule.provider('commonConfig', function()
    {
        // This runs at the config phase of the application, before the "run" phase
        this.config = {

            // These are the properties we need to set
            // controllerActivateSuccessEvent: '',
            // spinnerToggleEvent: '' 

        };

        this.$get = function()
        {
            return {
                config: this.config
            };
        };
    });

    //#region Additional info
    // INFO: we could write cm = angular.module(..., []) then cm.factory('logger'..., cm.factory('spinner'...
    // to keep them i seperate files we write angular.module('common').factory(......) in a seperate file


    //AFTER CONFIGURATION, THEN THE SERVICES ARE RUN
    // -> common service
    // -> logger service
    // -> spinner service
    // etc
    //#endregion
}