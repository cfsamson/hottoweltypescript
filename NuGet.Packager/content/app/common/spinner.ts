﻿/// <reference path="common.ts" />
/// <reference path="commonconfig.ts" />
'use strict';
module Application.Shared
{
    // Must configure the common service and set its 
    // events via the commonConfigProvider

    angular.module('common')
        .factory('spinner', ['common', 'commonConfig', (common, commonConfig)=>new Spinner(common, commonConfig)]);

    export interface ISpinner {
        spinnerHide(): void
        spinnerShow(): void
    }

    export class Spinner implements ISpinner{
        common: ICommon;
        commonConfig: any;

        constructor(common: ICommon, commonConfig:any)
        {
            this.common = common;
            this.commonConfig = commonConfig;
        }

        spinnerHide():void
        {
            this.spinnerToggle(false);
        }

        spinnerShow():void
        {
            this.spinnerToggle(true);
        }

        private spinnerToggle(show:boolean):void
        {
            this.common.$broadcast(this.commonConfig.config.spinnerToggleEvent, { show: show });
        }


    }

}