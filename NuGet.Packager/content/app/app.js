﻿'use strict';
var Application;
(function (Application) {
    var app = angular.module('app', [
        'ngAnimate',
        'ngRoute',
        'ngSanitize',
        'common',
        'common.bootstrap',
        'breeze.angular',
        'breeze.directives',
        'ui.bootstrap'
    ]);

    // Handle routing errors and success events
    app.run([
        '$route', function ($route) {
            // Include $route to kick start the router.
        }]);
})(Application || (Application = {}));
//# sourceMappingURL=app.js.map
