﻿/// <reference path="../common/common.ts" />
'use strict';

module Application.Controllers{
    var controllerId = 'admin';
    angular.module('app').controller(controllerId, ['common', (common)=>new Admin(common, controllerId)]);
    
    export interface IAdmin {
        common:Application.Shared.ICommon;
        controllerId: string;
        title:string;
    }

    export class Admin implements IAdmin {

        //#region variables
        common: Application.Shared.ICommon;
        controllerId: string;
        private log: Function;
        title: string;
        //#endregion
        constructor(common: Application.Shared.ICommon, controllerId: string)
        {
            this.common = common;
            this.controllerId = controllerId;
            this.title = "Admin";
            this.log = this.common.logger.getLogFn(controllerId);
            this.activate([]);
        } 
        //#region private methods
        private activate(promises:Array<ng.IPromise<any>>):void
        {
            this.common.activateController([], controllerId)
                .then(() => { this.log('Activated Admin View'); });
        }
        //#endregion
    }

}