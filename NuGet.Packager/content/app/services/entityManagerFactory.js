﻿/// <reference path="../../scripts/typings/breeze/breeze.d.ts" />
'use strict';
var Application;
(function (Application) {
    (function (Services) {
        var serviceId = 'entityManagerFactory';
        angular.module('app').factory(serviceId, [
            'breeze', 'config',
            function (breeze, config) {
                return new EntityManagerFactory(breeze, config);
            }
        ]);

        var EntityManagerFactory = (function () {
            function EntityManagerFactory(breeze, config) {
                this.breeze = breeze;
                this.config = config;
                this.setNamingConventionToCamelCase();
                this.preventValidateOnAttach();
                this.metadataStore = new breeze.MetadataStore();
                this.serviceName = config.remoteServiceName;
            }
            EntityManagerFactory.prototype.newManager = function () {
                var mgr = new breeze.EntityManager({
                    serviceName: this.serviceName,
                    metadataStore: this.metadataStore
                });

                return mgr;
            };

            EntityManagerFactory.prototype.setNamingConventionToCamelCase = function () {
                // Convert server - side PascalCase to client - side camelCase property names
                breeze.NamingConvention.camelCase.setAsDefault();
            };

            EntityManagerFactory.prototype.preventValidateOnAttach = function () {
                new breeze.ValidationOptions({ validateOnAttach: false }).setAsDefault();
            };
            return EntityManagerFactory;
        })();
        Services.EntityManagerFactory = EntityManagerFactory;
    })(Application.Services || (Application.Services = {}));
    var Services = Application.Services;
})(Application || (Application = {}));
//# sourceMappingURL=entityManagerFactory.js.map
